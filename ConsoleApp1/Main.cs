﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;


namespace ConsoleApp1
{
    class Main
    {
        public int totalBooks;
        public int totalLibrarys;
        public int totalDays;
        public List<int> score = new List<int>();
        public List<Library> libraries=new List<Library>();
        public List<Library> signedLibraries = new List<Library>();
        public List<Library>  librariesRemaining= new List<Library>();
        public List<Book> bookRemaining = new List<Book>();
        public List<Book> totalBooksList   = new List<Book>();

        public Main()
        {
        }

        public void Start()
        {
            StreamReader ficheroEntrada;
            StreamWriter ficheroSalida;


            ficheroEntrada = File.OpenText("prueba.txt");

            //lectura de primera
            GetFirstLine(ficheroEntrada.ReadLine());
            //lectura de scores
            GetSecondLine(ficheroEntrada.ReadLine());


            string linea1;
            string linea2;
            int idsBooks = 0;
            do
            {
             
                linea1 = ficheroEntrada.ReadLine();
                linea2 = linea1 != null ? ficheroEntrada.ReadLine() : null;
                if (linea1 != null && linea2 !=null)
                {
                    this.generateLibrary(linea1, linea2,idsBooks);
                }
                idsBooks++;

            }
            while (linea1 != null && linea2 != null);
            ficheroEntrada.Close();

            this.CreateSignedList();

             ficheroSalida = File.CreateText("prueba_output.txt");

            ficheroSalida.WriteLine(this.signedLibraries.Count);
            
            for (int i=0; i<this.signedLibraries.Count; i++)
            {
                Library currentLibrary = this.signedLibraries[i];

                ficheroSalida.WriteLine(currentLibrary.id + " " + currentLibrary.books.Count);
                ficheroSalida.WriteLine(currentLibrary.GenerateBooksString());

            }
            ficheroSalida.Close();
        }


        public void generateLibrary(string linea1, string linea2,int id)
        {
            Library newLibrary = new Library( linea1,linea2,this.score,id);
            this.totalBooksList.AddRange(newLibrary.books);
            this.libraries.Add(newLibrary);
        }
        public void GetFirstLine(string linea)
        {
            string[] lineas = linea.Split(' ');
            this.totalBooks = Convert.ToInt32(lineas[0]);
            this.totalLibrarys = Convert.ToInt32(lineas[1]);
            this.totalDays = Convert.ToInt32(lineas[2]);

        }

        public void GetSecondLine(string linea)
        {
            string[] lineas = linea.Split(' ');

            for(int i=0;i<lineas.Length; i++)
            {
                this.score.Add(Convert.ToInt32(lineas[i]));
            }

        }


        public void CreateSignedList()
        {
            this.librariesRemaining = this.libraries;
            this.bookRemaining = this.totalBooksList;
            this.librariesRemaining.Sort();
            int k = 0;
            do
            {
              
                this.signedLibraries.Add(this.librariesRemaining[0]);
                
                this.librariesRemaining.Remove(this.librariesRemaining[0]);
               
                for(int i=k; i < this.signedLibraries.Count; i++)
                {

                
                    
                    for (int j=0; j < this.librariesRemaining.Count; j++)
                    {
                        
                        for (int x = 0; x < this.signedLibraries[i].books.Count; x++)
                        {
                           
                            for (int y = 0; y < this.librariesRemaining[j].books.Count; y++)
                            {
                                if(this.signedLibraries[i].books[x].id == this.librariesRemaining[j].books[y].id)
                                {
                                   
                                    this.librariesRemaining[j].books.Remove(this.librariesRemaining[j].books[y]);
                                }
                            }                        }
                        this.librariesRemaining[j].CalculatePpd();
                    }

                }
                k++;
                /*
                List<Book> booksToRemove = this.librariesRemaining[0].books;

                this.librariesRemaining.Remove(this.librariesRemaining[0]);

                for(int i=0; i < this.librariesRemaining.Count; i++)
                {
                    for(int j=0; j < booksToRemove.Count; j++)
                    {
                        for(int k=0; k < this.librariesRemaining[i].books.Count; k++)
                        {
                            if(this.librariesRemaining[i].books[k] == booksToRemove[j])
                            {
                                this.librariesRemaining[i].books.Remove(this.librariesRemaining[i].books[k]);
                            }
                        }

                    }
                    this.librariesRemaining[i].CalculatePpd();
                }
                */
               
                this.librariesRemaining.Sort();
                if (this.librariesRemaining.Count <= 0 || this.librariesRemaining[0].books.Count <= 0) break;
            } while(this.librariesRemaining.Count > 0);

            Console.Write("Fin");
        }

    }
}
