﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace ConsoleApp1
{
    class Book :IComparable<Book>
    {
        public int id;
        public int score;

        public Book(int score, int id)
        {
            this.id = id;
            this.score = score;
        }

        public int CompareTo([AllowNull] Book other)
        {
            return this.id.CompareTo(other.id);
        }


    }
}
