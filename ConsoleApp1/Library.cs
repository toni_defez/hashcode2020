﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using System.Linq;

namespace ConsoleApp1
{
  
    class Library : IComparable<Library>
    {
        public List<Book> books;
        public int id;
        public int singTime;
        public int scanTime;
        public int totalBooks;
        public double ppd; //points per day

     
     

        public Library(string linea1, string linea2, List<int>scores, int id)
        {
            string[] lineas1 = linea1.Split(' ');
            string[] lineas2 = linea2.Split(' ');

            this.totalBooks = Convert.ToInt32(lineas1[0]);
            this.singTime = Convert.ToInt32(lineas1[1]);
            this.scanTime = Convert.ToInt32(lineas1[2]);
            this.id = id;

            this.books = new List<Book>();

            for(int i=0; i < lineas2.Length; i++)
            {
                int idBook = Convert.ToInt32(lineas2[i]);
                Book currentBook = new Book(scores[idBook], idBook);

                if (!this.books.Contains(currentBook)){
                    this.books.Add(currentBook);
                }
                
            }

            this.books.Sort();
            CalculatePpd();
        }

        public Library(List<Book> books, int id, int singTime, int scanTime)
        {
            this.books = books;
            this.id = id;
            this.singTime = singTime;
            this.scanTime = scanTime;
        }

        public void CalculatePpd()
        {
           
            int totalScore = 0;
            for(int i=0; i<this.books.Count; i++){
                totalScore += this.books[i].score;
            }
            this.ppd = (double) totalScore / ((
                (double)this.books.Count / this.scanTime) + this.singTime);
        }

        public string GenerateBooksString()
        {
            string line = "";
            for (int i = 0; i < this.books.Count; i++)
            {
                line += this.books[i].id +" ";
            }
            return line;
        }

        

         
        public void ReCalculatePpd(Library library)
        {
            this.ppd = 0;
        }

        public int CompareTo([AllowNull] Library other)
        {
            return other.ppd.CompareTo(this.ppd);
        }
    }
}
